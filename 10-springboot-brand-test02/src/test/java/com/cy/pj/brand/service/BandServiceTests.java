package com.cy.pj.brand.service;


import com.cy.pj.brand.pojo.Brand;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class BandServiceTests {
    @Autowired
    private BrandService brandService;

    @Test
    void testServices(){
        List<Brand> list = brandService.findBrands("t");
        for (Brand b: list
             ) {
            System.out.println("/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//*/*");
            System.out.println(b);

        }
    }

}
