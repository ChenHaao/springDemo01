package com.cy.pj.brand.service;

import com.cy.pj.brand.pojo.Brand;

import java.util.List;


public interface BrandService {
    List<Brand> findBrands(String name);

    int deleteById(Integer id);

    int saveBrand(Brand brand);

    Brand findById(Integer id);

    int updateBrand(Brand brand);
}
