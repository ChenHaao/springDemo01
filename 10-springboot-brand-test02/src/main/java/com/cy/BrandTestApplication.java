package com.cy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrandTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(BrandTestApplication.class, args);
    }

}
