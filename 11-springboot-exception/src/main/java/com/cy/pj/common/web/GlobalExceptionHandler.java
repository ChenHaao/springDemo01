package com.cy.pj.common.web;


import com.cy.pj.common.web.exception.ServiceException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//@ControllerAdvice //此注解处理的类是全局异常处理类
//@ResponseBody
@RestControllerAdvice   //@RestControllerAdvice==@ControllerAdvice  + @ResponseBody
public class GlobalExceptionHandler {
    //ExceptionHandler是描述处理异常的方法的注解
    //ExceptionHandler内部定义的异常类型为它描述的方法可以处理的异常类型
    @ExceptionHandler({ArithmeticException.class})
    @ResponseBody
    public String doHandleArithmeticException(ArithmeticException e){
        return "所出现的异常信息是:" + e.getMessage();
    }

    @ExceptionHandler({ServiceException.class})
    @ResponseBody
    public String doHandleServiceException(ServiceException e){
        return "所出现的异常信息是:" + e.getMessage();
    }

    @ExceptionHandler({ArrayIndexOutOfBoundsException.class})
    public String doHandleArrayIndexOutOfBoundsExceptionException(ArrayIndexOutOfBoundsException e){
        return "数组下标越界";
    }

    @ExceptionHandler({RuntimeException.class})
    public String doHandleRuntimeException(RuntimeException e){
        e.printStackTrace();

        return "runtime exception "+e.getMessage();
    }

    @ExceptionHandler({Throwable.class})
    public String doHandleThrowable(Throwable e){  //此方法为异常处理的兜底方法
        e.printStackTrace();
        //log.error(...);
        //给运维人员发短信
        //发运维人员发email
        //播放一段音乐(报警)
        return "系统出大问题咯";
    }


}
