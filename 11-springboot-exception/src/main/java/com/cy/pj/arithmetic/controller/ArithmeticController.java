package com.cy.pj.arithmetic.controller;

import com.cy.pj.arithmetic.service.ArithemeticService;
import com.cy.pj.common.web.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.relational.core.sql.In;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ArithmeticController {


    @ResponseBody
    @RequestMapping("/doCompute/{n1}/{n2}")
    public String doCompute(@PathVariable Integer n1, @PathVariable Integer n2) {

        Integer result = n1 / n2;
        return "结果是" + result;

    }

    @Autowired
    private ArithemeticService arithemeticService;

    @ResponseBody
    @RequestMapping("doCompute/{a}/{b}")
    public String doSum(@PathVariable Integer a,@PathVariable Integer b) {
        return "计算结果为:" + arithemeticService.sum(a, b);
    }

    @ResponseBody
    @RequestMapping("doSum/{num}")
    public String doSum(@PathVariable Integer...num) {
        return "计算结果为:" + arithemeticService.sum(num);
    }


}
