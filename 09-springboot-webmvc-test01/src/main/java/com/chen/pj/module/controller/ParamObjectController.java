package com.chen.pj.module.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ParamObjectController {

    @RequestMapping("/doParam02")
    public String doMethodParam(RequestParam name) {
        return "request +" + name;
    }
}
