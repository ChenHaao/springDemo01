package com.chen.pj.module.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/*@Controller注解标注类的方法，return时会被视图处理器识别成静态文件的路径。
默认为templates文件夹下。如return "test/hello"表示的是默认路径下的test文件
夹中的名叫hello的文件，带上后缀名.html或btl等也可以识别。*/
@Controller
public class ModelViewController {

    @RequestMapping("/doModulAndView")
    public String doModulAndView(Model model){
        model.addAttribute("name","陈浩");
        model.addAttribute("job","学生");
        return "default";
    }

}
