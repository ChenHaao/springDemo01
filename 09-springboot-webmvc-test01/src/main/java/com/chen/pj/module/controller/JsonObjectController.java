package com.chen.pj.module.controller;

import com.chen.pj.module.pojo.RequestParameter;
import com.chen.pj.module.pojo.ResponseResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@RestController
public class JsonObjectController {
    @RequestMapping("/doConvertResponseToJson")
    public ResponseResult doConvertResponseToJson(){
        ResponseResult rs = new ResponseResult();
        rs.setAge(20);
        rs.setName("陈浩");
        return rs;
    }
    @RequestMapping("/doConvertMapToJson")
    public Map doConvertMapToJson(){
        Map m = new HashMap();
        m.put("name","王思聪");
        m.put("age",12);
        return m;
    }
    @RequestMapping("/doPrint")
    public void doPrint(HttpServletResponse response) throws Exception {
        Map m = new HashMap();
        m.put("name","zjq");
        m.put("age",18);
        //将map中的数据转换为json格式字符串
        ObjectMapper mp = new ObjectMapper();
        String jsonStr = mp.writeValueAsString(m);
        System.out.println("jsonStr= "+jsonStr);
        response.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        PrintWriter pw = response.getWriter();
        pw.println(jsonStr);

    }

    @RequestMapping("/doMethodParam")
    public String doMethodParam(RequestParameter name){

        return "request name=" + name.toString();
    }

    @RequestMapping("/doMethodParam02")
    public String doMethodParam02(@RequestParam Map param){
        return "request params" + param.toString();

    }

}
