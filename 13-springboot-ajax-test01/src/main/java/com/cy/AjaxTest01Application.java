package com.cy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AjaxTest01Application {

    public static void main(String[] args) {
        SpringApplication.run(AjaxTest01Application.class, args);
    }

}
