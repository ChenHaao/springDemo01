package com.cy.ajax.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AjaxController {

    @RequestMapping("doAjaxStart")
    @CrossOrigin
    public String doAjaxGet(){
        System.out.println("======doAjaxGet======");
        return "AjaxController中的doAjaxGet执行了！";
    }


}
