function ajaxGet(url,params,callback) {
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState==4){
            if (xhr.status==200){
                callback(xhr.responseText);
            }
        }
    }
    xhr.open("GET",params?url+"?"+params:url,true);
    xhr.send(null);

}