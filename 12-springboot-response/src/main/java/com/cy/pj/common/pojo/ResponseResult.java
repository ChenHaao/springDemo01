package com.cy.pj.common.pojo;


/** 封装服务端响应到客户端的结果*/
public class ResponseResult {
    /**响应状态码*/
    private Integer state=1;  // 1表示ok  0表示error
    /**响应状态码对应的具体信息*/
    private String message = "OK!";
    /**响应数据（一般是查询时获取到的正确数据）*/
    private Object data;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ResponseResult(String message) {  //new ResponseResult("delete ok!")
        this.message = message;
    }

    public ResponseResult(Throwable e){
        this.state=0;
        this.message=e.getMessage();
    }

    @Override
    public String toString() {
        return "ResponseResult{" +
                "state=" + state +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
