package com.cy.pj.category.dao;


import com.cy.pj.category.pojo.Category;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface CategoryDao {
    @Select("select * from tb_category")
    List<Category> findCategorys();

//    @Delete("delete from tb_brand where id=#{id}")
//    int deleteById(Integer id);
//
//    int insertBrand(Category brand);
//
//    @Select("select * from tb_brand where id=#{id}")
//    Category findById(Integer id);
//
//    @Update("update tb_brand set name=#{name},remark=#{remark} where id=#{id}")
//    int updateBrand(Category Brand);
}
