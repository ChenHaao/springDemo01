package com.cy.pj.category.service.imp;

import com.cy.pj.category.dao.CategoryDao;
import com.cy.pj.category.pojo.Category;
import com.cy.pj.category.service.CategoryService;
import com.cy.pj.common.annotation.RequiredCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CategoryServiceImp implements CategoryService {

    @Autowired
    private CategoryDao categoryDao;

    @RequiredCache
    @Override
    public List<Category> findCategorys() {
        System.out.println("findCategorys执行了");

        return categoryDao.findCategorys();
    }
}
