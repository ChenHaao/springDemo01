package com.chen.pj.dao;


import com.chen.pj.pojo.Brand;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BrandDao {
    List<Brand> findAllBrand();

}
