package com.chen.pj.goods.service;

import com.chen.pj.goods.pojo.Goods;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class GoodsServiceTests {
    @Autowired
    private GoodsService goodsService;

    @Test
    void testGoodsService(){
        List<Goods> list = goodsService.findGoods();
        for (Goods g:list) {
            System.out.println("**************");
            System.out.println(g);
        }
    }

}
