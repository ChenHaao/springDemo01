package com.chen.pj.goods.service;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LogTests {
    private static final Logger log= LoggerFactory.getLogger(LogTests.class);

    @Test
    void testsLogLevel(){
        log.info("log level=info!");
    }
}
