package com.chen.pj.goods.dao;

import com.chen.pj.goods.pojo.Goods;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class GoodsDaoTests {
    @Autowired

    private GoodsDao goodsDao;

    @Test
    void testFindGoods() {
        List<Goods> list = goodsDao.findGoods();
        for (Goods l : list
        ) {
            System.out.println("输出goods对象:");
            System.out.println(l);

        }
    }

}
