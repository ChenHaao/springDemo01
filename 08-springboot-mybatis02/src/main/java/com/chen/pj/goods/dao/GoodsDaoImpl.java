package com.chen.pj.goods.dao;

import com.chen.pj.goods.pojo.Goods;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository //描述数据层实现类,用于交给spring管理
public class GoodsDaoImpl implements GoodsDao {
    @Autowired
    private SqlSession sqlSession;

    @Override
    public List<Goods> findGoods() {
        return sqlSession.getMapper(GoodsDao.class).findGoods();
    }
}
