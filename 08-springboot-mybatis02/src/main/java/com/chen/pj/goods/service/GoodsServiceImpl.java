package com.chen.pj.goods.service;

import com.chen.pj.goods.dao.GoodsDao;
import com.chen.pj.goods.pojo.Goods;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsServiceImpl implements GoodsService {
    private static final Logger log = LoggerFactory.getLogger(GoodsServiceImpl.class);

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public List<Goods> findGoods() {
        Long t1 = System.currentTimeMillis();
        List<Goods> list = goodsDao.findGoods();
        Long t2 = System.currentTimeMillis();
        log.info("运行时间:{}",t2-t1);
        return list;
    }
}
