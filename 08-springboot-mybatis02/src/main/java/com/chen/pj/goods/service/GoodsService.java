package com.chen.pj.goods.service;

import com.chen.pj.goods.pojo.Goods;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


public interface GoodsService {
    List<Goods> findGoods();
}
