package com.cy.pj.common.pojo;


import lombok.Data;

import java.io.Serializable;

/**通过此对象封装类似CheckBox的结构的数据*/
@Data
public class CheckBox implements Serializable {
    private static final long serialVersionUID = -4429350447358550306L;

    private Integer id;
    private String name;


}
