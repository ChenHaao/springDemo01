package com.cy.pj.common.cache;


import java.util.Objects;

public class SimpleCacheKey {

    private Object params;

    public SimpleCacheKey(Object params) {
        this.params = params;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleCacheKey that = (SimpleCacheKey) o;
        return Objects.equals(params, that.params);
    }

    @Override
    public int hashCode() {
        return Objects.hash(params);
    }



}
