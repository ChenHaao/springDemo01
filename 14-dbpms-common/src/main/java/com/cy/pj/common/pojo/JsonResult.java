package com.cy.pj.common.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 借助此对象封装在控制层响应到客户端的数据
 */

@Data
@AllArgsConstructor

public class JsonResult implements Serializable {
    /**
     * 响应状态码
     */
    private Integer state = 1;
    /**
     * 响应状态码对应的具体信息
     */
    private String message = "ok";
    /**
     * 响应数据
     */
    private Object data;

    public JsonResult() {
    }

    public JsonResult(String message) {
        this.message = message;
    }

    public JsonResult(Object data) {
        this.data = data;
    }

    public JsonResult(Throwable e) {
        this.message = e.getMessage();
    }
}
