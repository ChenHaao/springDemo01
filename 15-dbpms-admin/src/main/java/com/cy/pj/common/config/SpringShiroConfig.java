package com.cy.pj.common.config;


import com.cy.pj.common.pojo.JsonResult;
import org.apache.shiro.ShiroException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;

@Configuration  //此注解描述的类为spring中的配置类
public class SpringShiroConfig {
    /**
     * 构建并初始化SercurityManager对象 ,然后将此对象交给spring管理
     * 说明:@Bean注解应用于@Configuration注解描述的类的内部,通过此注解的方法,
     * 方法的返回值会交给spring管理
     * @return 返回值为shiro中的安全管理器对象,是shiro框架的核心,此对象中
     * 实现了认证,授权,会话,缓存,加密等一系列功能的实现
     *
     * */

    @Bean //bean的名字默认是方法名
    //@Scope("singleton") //作用域默认为单例的
    public SecurityManager securityManager(){
        DefaultWebSecurityManager SecurityManager
                = new DefaultWebSecurityManager();
        return SecurityManager;
    }

    /**构建并初始化ShiroFilterFactoryBean对象,通过此对象
     * 创建过滤器工厂,进而通过过滤器工厂创建过滤器(filter),
     * 并通过过滤器对请求新消息进行过滤,例如:检测此请求是否需要认证
     * 或此请求是否已认证.
     **/
    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager){
        ShiroFilterFactoryBean fBean  = new ShiroFilterFactoryBean();
        //设置securityManager,基于此对象进行验证检测
        fBean.setSecurityManager(securityManager);
        fBean.setLoginUrl("/doLoginUI");
        LinkedHashMap<String,String> map= new LinkedHashMap<>();
        //静态资源允许匿名访问:"anon"
        map.put("/bower_components/**","anon");
        map.put("/build/**","anon");
        map.put("/dist/**","anon");
        map.put("/plugins/**","anon");
        //除了匿名访问的资源,其它都要认证("authc")后访问
        map.put("/user/doLogin","anon");
        map.put("/doLogout","logout");
        map.put("/**","authc");
        fBean.setFilterChainDefinitionMap(map);
        return fBean;
    }




}
