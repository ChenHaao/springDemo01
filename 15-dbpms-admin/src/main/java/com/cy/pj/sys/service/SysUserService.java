package com.cy.pj.sys.service;

import com.cy.pj.common.pojo.PageObject;
import com.cy.pj.sys.pojo.SysUser;
import com.cy.pj.sys.pojo.SysUserDept;

import java.util.Map;

public interface SysUserService {

    Map<String,Object> findById(Integer id);

    int updateObject(SysUser entity,Integer[]roleIds);

    int saveObject(SysUser entity,Integer[]roleIds);
    /**
     * 禁用启用
     * @param id
     * @param valid
     * @return
     */
    int validById(Integer id,Integer valid);

    PageObject<SysUserDept> findPageObjects(String username,Integer pageCurrent);
}