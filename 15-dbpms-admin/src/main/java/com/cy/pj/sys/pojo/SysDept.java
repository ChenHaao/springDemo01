package com.cy.pj.sys.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
/**
 * 部门PO对象
 */
@Data
public class SysDept implements Serializable{
    private static final long serialVersionUID = 8876920804134951849L;
    private Integer id;
    private String name;
    private Integer parentId;
    private Integer sort;
    private String note;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date createdTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date modifiedTime;
    private String createdUser;
    private String modifiedUser;
}