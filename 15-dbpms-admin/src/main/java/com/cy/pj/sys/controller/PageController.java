package com.cy.pj.sys.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**借助PageController处理客户端所有页面请求*/
@RequestMapping("/")
@Controller
public class PageController {
    //http://localhost/menu/doFindObjects
    //http://localhost/log/log_list
    //http://localhost/menu/menu_list
    @RequestMapping("/{module}/{moduleUI}")
    public String doModuleUI(@PathVariable String moduleUI){
        return "sys/"+moduleUI;
    }

    @RequestMapping("doPageUI")
    public String doPageUI(){
        return "common/page";
    }
    @RequestMapping("doIndexUI")
    public String doIndexUI(){
        return "starter";
    }

    @RequestMapping("doLoginUI")
    public String doLoginUI(){
        return "login";
    }
}
