package com.cy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Dbpms15Application {

	public static void main(String[] args) {
		SpringApplication.run(Dbpms15Application.class, args);
	}

}
