package com.cy.pj.sys.dao;

import org.apache.ibatis.annotations.Param;

public interface SysRoleMenuDao {
    int insertObjects(@Param("roleId")Integer roleId, @Param("menuIds")Integer[] menuIds);


}
