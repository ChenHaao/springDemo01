package com.cy.pj.sys.service;

import com.cy.pj.common.pojo.PageObject;
import com.cy.pj.sys.pojo.SysRole;

public interface SysRoleService {
    /**
     * 基于角色名分页查询角色信息
     * @param name 角色名
     * @param pageCurrent 当前页码值
     * @return  查询到的当前页角色信息以及分页信息
     */
    PageObject<SysRole> findPageObjects(String name,Integer pageCurrent);
}