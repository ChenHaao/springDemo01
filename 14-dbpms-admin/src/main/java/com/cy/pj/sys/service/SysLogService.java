package com.cy.pj.sys.service;

import com.cy.common.bo.PageObject;
import com.cy.pj.sys.entity.SysLog;

public interface SysLogService {

    /**

     * @param name 基于条件查询时的参数名

     * @param pageCurrent 当前的页码值

     * @return 当前页记录+分页信息

     */

    PageObject<SysLog> findPageObjects(

            String username,

            Integer pageCurrent);

}
