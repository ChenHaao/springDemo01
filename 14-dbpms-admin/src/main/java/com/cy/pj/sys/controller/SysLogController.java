package com.cy.pj.sys.controller;

import com.cy.pj.common.vo.JsonResult;
import com.cy.common.bo.PageObject;
import com.cy.pj.sys.entity.SysLog;
import com.cy.pj.sys.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller

@RequestMapping("/log/")

public class SysLogController {

    @Autowired

    private SysLogService sysLogService;

    @RequestMapping("doFindPageObjects")

    @ResponseBody

    public JsonResult doFindPageObjects(String username, Integer pageCurrent){

        PageObject<SysLog> pageObject=

                sysLogService.findPageObjects(username,pageCurrent);

        return new JsonResult(pageObject);

    }

}