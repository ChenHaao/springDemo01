package com.cy.pj.sys.service.impl;

import com.cy.pj.common.exception.ServiceException;
import com.cy.pj.common.pojo.PageObject;
import com.cy.pj.sys.dao.SysRoleDao;
import com.cy.pj.sys.pojo.SysRole;
import com.cy.pj.sys.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysRoleServiceImpl implements SysRoleService {
    @Autowired
    private SysRoleDao sysRoleDao;
    @Override
    public PageObject<SysRole> findPageObjects(
            String name, Integer pageCurrent) {
        //1.参数校验
        if(pageCurrent==null||pageCurrent<1)
            throw new IllegalArgumentException("页码值不正确");
        //2.查询总记录数并校验
        int rowCount=sysRoleDao.getRowCount(name);
        if(rowCount==0)
            throw new ServiceException("没有找到对应记录");
        //3.查询当前页记录
        int pageSize=2;
        int startIndex=(pageCurrent-1)*pageSize;
        List<SysRole> records=sysRoleDao.findPageObjects(name,startIndex,pageSize);
        //4.封装查询结果并返回
//        PageObject pageObject=new PageObject<>();
//        pageObject.setRowCount(rowCount);
//        pageObject.setRecords(records);
//        pageObject.setPageSize(pageSize);
//        pageObject.setPageCurrent(pageCurrent);
//        int pageCount=rowCount/pageSize;
//        if(rowCount%pageSize!=0)pageCount++;
//        pageObject.setPageCount(pageCount);
//        return pageObject;

        return new PageObject<>(rowCount,records,pageSize,pageCurrent);

    }
}