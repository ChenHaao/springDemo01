package com.cy.pj.sys.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("{module}/{moduleUI}")
public class doModuleUI {

    public String doModuleUI(@PathVariable String moduleUI){
        return "sys/"+moduleUI;
    }
}
