package com.cy.pj.sys.service;



import com.cy.common.bo.PageObject;
import com.cy.pj.sys.entity.SysLog;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SysLogServiceTests {

    @Autowired
    private SysLogService sysLogService;

    @Test
    public void testFindPageObjects() {
        PageObject<SysLog> pageObject = sysLogService.findPageObjects("admin", 1);
        System.out.println(pageObject);
    }
}
