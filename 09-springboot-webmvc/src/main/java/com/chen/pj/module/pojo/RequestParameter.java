package com.chen.pj.module.pojo;

/**基于此对象在服务器*/
public class RequestParameter {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "RequestParameter{" +
                "name='" + name + '\'' +
                '}';
    }
}
