package com.chen.pj.module.controller;


import com.chen.pj.module.pojo.RequestParameter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController  //@RestController 内部返回的是json格式的字符串
public class ParamObjectController {
    // http://localhost/doParam01?name=ss
    @GetMapping("/doParam01")//这个注解描述的方法只能处理get类型的请求
    public String doMethodParam(String name){

        return "request params=" +name;
    }

    @GetMapping("/doParam02")
    public String doMethodParam(RequestParameter param){

        return "request params=" +param.toString();
    }

    // http://localhost/doParam03?name=ss
    @GetMapping("/doParam03")
    public String doMethodParam(@RequestParam Map<String,Object> param){
        // 当方法参数是map类型，且希望基于map接受客户端请求参数，
        // 则需要使用指定注解对map进行描述，例如@RequestParam
        // 但是，使用map作为方法参数接受客户端请求参数不严谨（客户端可以传任意参数）
        return "request params=" +param.toString();
    }
}
