package com.chen.pj.module.controller;

import com.chen.pj.module.pojo.ResponseResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;


@RestController   //@RestController = @Controller +@ResponseBody
public class JsonObjectController {

    @RequestMapping("/doConvertResponseToJson")
    @ResponseBody
    public ResponseResult doConverResponseToJason(){
        ResponseResult r= new ResponseResult();
        r.setCode(200);
        r.setMessage("update ok");
        return r;
    }

    @ResponseBody
    @RequestMapping("/doPrintJson")
    public void doPrintJson(HttpServletResponse response) throws Exception {
        Map map = new HashMap();
        map.put("username","刘德华");
        map.put("state","退役歌星");
        String jsonStr = new ObjectMapper().writeValueAsString(map);
        response.setCharacterEncoding("utf-8");
        //设置响应到客户端的内容类型，且告诉客户端以什么编码进行显示。
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().println(jsonStr);


    }


}


