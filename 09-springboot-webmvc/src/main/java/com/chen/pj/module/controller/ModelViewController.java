package com.chen.pj.module.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ModelViewController {

    @RequestMapping("/doModelAndView")
    public String doModelAndView(Model model){
        // 参数model由spring web模块创建，可以存储一些数据
        model.addAttribute("username","chenhao");
        model.addAttribute("state","perfect");
        return "view"; //viewname
        // 这里的响应结果，会在spring mvc中会帮我们封装为ModelAndview对象

    }

    @RequestMapping("/doModelAndView02")
    public ModelAndView doModelAndView(){
        ModelAndView mv = new ModelAndView();

        mv.addObject("username","chenhao02");
        mv.addObject("state","ok02");
        mv.setViewName("view");

        return mv;

    }


}
