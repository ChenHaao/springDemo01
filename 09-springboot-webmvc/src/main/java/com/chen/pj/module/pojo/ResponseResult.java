package com.chen.pj.module.pojo;

/*基于此对象封装响应结果*/
public class ResponseResult {
    /*响应状态码*/
    private Integer code;
    /*相应信息*/
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ReponseResult{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
