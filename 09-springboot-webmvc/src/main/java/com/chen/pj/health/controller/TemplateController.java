package com.chen.pj.health.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller   //bean对象
public class TemplateController { //在spring mvc中这个对象称之为handler(处理器)

    //访问:  http://localhost/doTemplateUI
    @RequestMapping("doTemplateUI")
//    @ResponseBody
    public  String doTemplateUI(){ //@RequestMapping用于定义url请求的映射
        return "default";  //view name(视图名)
        //1 这名字返回给谁了?方法的调用者(调用者是---DispatcherServlet)
        //2 DispatcherServlet拿到viewname 以后做什么(交给师徒解释器进行解析-->前缀,后缀,数据)
        //3 将拿到的view解析结果响应到客户端(/templates/default.html)
    }

}
